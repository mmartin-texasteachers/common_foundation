defmodule CommonFoundation.Router do
  use CommonFoundation.Web, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/api", CommonFoundation do
    pipe_through :api
  end
end
