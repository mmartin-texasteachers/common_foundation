# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :common_foundation,
  ecto_repos: [CommonFoundation.Repo]

# Configures the endpoint
config :common_foundation, CommonFoundation.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "YbcOvWtiHJU2QIaq6HOXgU2VidbOqkLVHeRBuE1/lyCoNk+kEb2RKLfr6fqywbXk",
  render_errors: [view: CommonFoundation.ErrorView, accepts: ~w(json)],
  pubsub: [name: CommonFoundation.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
